﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace SteamBot
{
    public class Log
    {
        public bool debugging { get; set; }
        private String logPath = "";
        protected StreamWriter fileStream;

        private ConsoleColor default_fg_colour;

        public Log(String Path, bool debug = false)
        {
            default_fg_colour = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.White;

            logPath = Path;
            fileStream = File.AppendText(logPath);
            fileStream.AutoFlush = true;

            fileStream.WriteLine(GetDateTime() + " - Application start.");

            debugging = debug;
        }
		
        public void Info(String message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("INFO: " + message);
            LogToFile("INFO: " + message);
        }

        public void Info(String format, params Object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("INFO: " + format, args);
            ResetConsole();
            LogToFile("INFO: " + format, args);
        }

        public void Success(String message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("SUCCESS: " + message);
            ResetConsole();
            LogToFile("SUCCESS: " + message);
        }

        public void Success(String format, params Object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("SUCCESS: " + format, args);
            ResetConsole();
            LogToFile("SUCCESS: " + format, args);
        }

        public void Error(String message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR: " + message);
            ResetConsole();
            LogToFile("ERROR: " + message);
        }

        public void Error(String format, params Object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR: " + format, args);
            ResetConsole();
            LogToFile("ERROR: " + format, args);
        }

        public void Warn(String message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("WARNING: " + message);
            ResetConsole();
            LogToFile("WARNING: " + message);
        }

        public void Warn(String format, params Object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("WARNING: " + format, args);
            ResetConsole();
            LogToFile("WARNING: " + format, args);
        }

        public void Debug(String message)
        {
            if (debugging)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("DEBUG: " + message);
                ResetConsole();
            }

            LogToFile("DEBUG: " + message);
        }

        public void Debug(String format, params Object[] args)
        {
            if (debugging)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("DEBUG: " + format, args);
                ResetConsole();
            }

            LogToFile("DEBUG: " + format, args);
        }

        private void LogToFile(String message)
        {
            fileStream.WriteLine(GetDateTime() + " - " + message);
        }

        private void LogToFile(String format, params Object[] args)
        {
            fileStream.WriteLine(GetDateTime() + " - " + format, args);
        }

        private void ResetConsole()
        {
            Console.ForegroundColor = default_fg_colour;
        }

        public void Close()
        {
            fileStream.Close();
            fileStream.Dispose();

            Console.ForegroundColor = default_fg_colour;
        }
		
		 private String GetDateTime()
        {
            return DateTime.Now.ToString();
        }
    }
}

