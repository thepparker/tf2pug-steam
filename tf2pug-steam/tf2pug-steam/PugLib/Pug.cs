﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SteamKit2;

namespace SteamBot.PugLib
{
    class Pug
    {
        public long id { get; set; }

        public int size { get; set; }

        public Player admin { get; set; }

        public EPugState state { get; set; } 

        public long map_vote_start { get; set; }
        public long map_vote_end { get; set; }
        public string map { get; set; }
        public bool map_forced { get; set; }
        public List<string> maps { get; set; }

        public string mumble { get; set; }

        private Dictionary<long, string> player_votes;
        private Dictionary<string, int> map_vote_counts;

        private List<Player> players;

        private List<Player> team_red;
        private List<Player> team_blue;
        
        /** Server details */
        public String ip { get; set; }
        public int port { get; set; }
        public String password { get; set; }

        public Pug()
        {
            players = new List<Player>();

            team_red = new List<Player>();
            team_blue = new List<Player>();

            player_votes = new Dictionary<long, string>();
            map_vote_counts = new Dictionary<string, int>();

            maps = new List<string>();
        }

        //----------------------------------------------
        // MAP VOTING
        //----------------------------------------------

        public bool VoteInProgress
        {
            get { return this.state == EPugState.MAP_VOTING; }
        }

        public bool VotingTimeEnded(long current_time)
        {
            return current_time > map_vote_end;
        }

        public void AddMapVote(long player_id, string map)
        {
            player_votes.Add(player_id, map);
        }

        public void SetMapVoteCount(string map, int count)
        {
            map_vote_counts.Add(map, count);
        }

        //----------------------------------------------
        // PLAYER MANIPULATION & INFORMATION
        //----------------------------------------------

        public void Add(SteamID user, string name)
        {
            Player player = new Player(user, name);
        }

        public void Add(Player player)
        {
            this.players.Add(player);
        }

        public void Remove(Player player)
        {
            this.players.Remove(player);
        }

        public String SlotsRemaining
        {
            get
            {
                return String.Format("{0}/{1} slots remaining", size - players.Count, size);
            }
        }

        public void AddRedMember(long player_id)
        {
            Player player = players.Find(p => p.SID.ConvertToUInt64() == (ulong)player_id);

            team_red.Add(player);
        }

        public void AddBlueMember(long player_id)
        {
            Player player = players.Find(p => p.SID.ConvertToUInt64() == (ulong)player_id);

            team_blue.Add(player);
        }

        //----------------------------------------------
        // TEAMS
        //----------------------------------------------

        public void ShuffleTeams()
        {
            team_red.Clear();
            team_blue.Clear();

            team_red.Add(players[0]);
            team_red.Add(players[1]);
            team_red.Add(players[2]);
            team_red.Add(players[3]);
            team_red.Add(players[4]);
            team_red.Add(players[5]);

            team_blue.Add(players[6]);
            team_blue.Add(players[7]);
            team_blue.Add(players[8]);
            team_blue.Add(players[9]);
            team_blue.Add(players[10]);
            team_blue.Add(players[11]);
        }

        public List<Player> TeamRed
        {
            get { return this.team_red; }
        }

        public List<Player> TeamBlue
        {
            get { return this.team_blue; }
        }

        //----------------------------------------------
        // HELPERS
        //----------------------------------------------

        public String GetStatusMessage()
        {
            if (state == EPugState.MAP_VOTING)
            {
                return "Map voting currently in progress";
            }
            else if (state == EPugState.GAME_STARTED)
            {
                return "The game has started";
            }
            else if (state == EPugState.GATHERING_PLAYERS)
            {
                return String.Format("Gathering players. ({0})", SlotsRemaining);
            }
            else if (state == EPugState.MAPVOTE_COMPLETE)
            {
                return "Details have been sent. Waiting for the game to begin";
            }
            else if (state == EPugState.GAME_OVER)
            {
                return "The game is over";
            }
            else
            {
                return "Uknown";
            }
        }

        public bool Started
        {
            get { return this.state == EPugState.GAME_STARTED; }
        }

        public int MapVoteCount(string map)
        {
            return map_vote_counts[map];
        }

        public List<Player> Players
        {
            get { return this.players; }
        }

        public bool Full
        {
            get { return this.size == this.players.Count; }
        }

        public String ConnectString
        {
            get
            {
                return String.Format("connect {0}:{1}; password {2}",
                                ip, port, password
                            );
            }
        }

        public String SteamConnectString
        {
            get
            {
                return String.Format("steam://{0}:{1}/{2}",
                        ip, port, password
                    );
            }
        }

        public String Maps
        {
            get { return String.Join(" - ", this.maps); }
        }
    }

    //----------------------------------------------
    // ENUMS
    //----------------------------------------------

    enum EPugState
    {
        GATHERING_PLAYERS = 0,
        MAP_VOTING = 1,
        MAPVOTE_COMPLETE = 2,
        GAME_STARTED = 3,
        GAME_OVER = 4
    }
}
