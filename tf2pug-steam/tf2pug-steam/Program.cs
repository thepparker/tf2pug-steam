﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace SteamBot
{
    class Program
    {
        public static PugBot bot;
        public static Log log;

        static List<PugBot> bot_list;

        /** 
         * Entry point. Establish client and user, setup callbacks, and 
         * run the bot
         * 
         * @param string[] args Command line arguments
         */
        static void Main(string[] args)
        {
            log = new Log("log.txt");
            bot_list = new List<PugBot>();

            Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
            {
                Program.log.Warn("Application Close. (KeyboardInterrupt)");
                e.Cancel = false;

                CleanUp();
            };

            // if we don't have 2 args, print error and usage message
            if (args.Length < 2)
            {
                log.Error("No username or password specified");
                print_usage();
                Console.ReadKey();
                return;
            }

            // if optional debug parameter is specified
            if (args.Length > 2)
            {
                bool debug = false;
                bool succeeded = Boolean.TryParse(args[2], out debug);

                if (succeeded)
                {
                    log.Info("Debugging: {0}", debug);
                }
                else
                {
                    log.Error("Debug parameter must be in format of 'True' or 'False'. Debugging disabled!");
                }

                log.debugging = debug;
            }

            new Thread(() =>
            {
                try
                {
                    bot = new PugBot(args[0], args[1]);
                    bot_list.Add(bot);
                }
                catch (Exception e)
                {
                    log.Error("ERROR CREATING BOT INSTANCE: " + e.Message);
                }
            }).Start();
        }

        /** Print simple usage message */
        static void print_usage()
        {
            log.Warn("Usage: tf2pug-steam [user] [pass] [debug]");
            log.Warn("debug parameter is optional and needs to be specified as true or false");
        }

        static void CleanUp()
        {
            log.Warn("Cleaning up.");

            foreach (var bot in bot_list)
            {
                bot.CleanUp();
            }

            log.Warn("Finished cleanup.");
            log.Close();
        }
    }
}
