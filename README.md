TF2Pug - Steam Edition (SE)
===========================

TF2Pug SE is a Steam client version of the once popular #tf2pug channel
on the GameSurge IRC network.

It aims to provide an automated and fluid service for users to find somewhat
serious games with other likeminded gamers, so they can improve their skills.

Unlike #tf2pug, the number of concurrent matches is limited solely by available
servers, and there is no game info spam in the main pug chat room. Furthermore,
since the bot is tied directly into Steam, user management is significantly
simpler.

Dependencies
------------

TF2Pug SE depends on the following libraries:
* SteamKit2 which can be found [here](https://github.com/SteamRE/SteamKit)
* JSON.Net which can be found [here](https://github.com/JamesNK/Newtonsoft.Json)

License
-------
TF2Pug SE is available under the [GPL 3.0 license](http://www.tldrlegal.com/license/gnu-general-public-license-v3-%28gpl-3%29)

Contact
-------

You can find bladez on the GameSurge IRC network as _bladez@bladezz.admin.ipgn,
on the ozfortress forums as bladez, or on the iPGN forums as bladez. However, 
your best bet is to submit an issue on this repository if you have any 
problems.
